#include "computerscientist.h"

const int notAvailible = -1;

computerScientist::computerScientist()
{
    name = "";
    gender = "N/A";
    birthYear = notAvailible;
    deathYear = notAvailible;
}

void computerScientist::edit(string sName, string sGender, int sBirthYear, int sDeathYear)
{
    name = sName;
    gender = sGender;
    birthYear = sBirthYear;
    deathYear = sDeathYear;
}

string computerScientist::getName()
{
    return name;
}

string computerScientist::getGender()
{
    return gender;
}

int computerScientist::getBirthYear()
{
    return birthYear;
}

int computerScientist::getDeathYear()
{
    return deathYear;
}

*listCommands*:     Lists all the commands that are availible
*add*:              Add a computer scientist to the list.
*remove*:           Remove an entry from the list.
*edit*              edit a scientist
*search*:           Search for a computer scientist in the list
*display*:          Display the list
*save*:             Saves the changes to the list
*clear*             Clears the console
*quit*:             Quit the program

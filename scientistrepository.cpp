#include "scientistrepository.h"

scientistRepository::scientistRepository()
{
    readRepository();
}

scientistRepository::~scientistRepository()
{
    writeToRepository();
}

void scientistRepository::overWriteList(vector<computerScientist> newList)
{
    list = newList;
}

computerScientist scientistRepository::getScientist(const int& listIndex)
{
    return list[listIndex];
}

vector<computerScientist> scientistRepository::getList()
{
    return list;
}

int scientistRepository::size()
{
    return list.size();
}

void scientistRepository::push(const computerScientist& scientist)
{
    list.push_back(scientist);
}

void scientistRepository::readRepository()
{
    ifstream fin;
    char character;
    string entryInput;

    string name;
    string gender;
    int birthYear;
    int deathYear;


    fin.open("repository.txt", ios::in);

    if(fin.fail())
        cout << "ERROR: could not open repository\n";

    else if(fin.is_open())
    {
        while(!fin.eof())
        {
            computerScientist entry;
            entryInput = "";

            fin.get(character);

            if(fin.eof())
                break;

            while(character != '\n' && !fin.eof())
            {
                entryInput += character;
                fin.get(character);
            }

            if(entryInput != "")
            {
                sortInput(entryInput, name, gender, birthYear, deathYear);
                entry.edit(name, gender, birthYear, deathYear);

                list.push_back(entry);
            }
        }

        fin.close();
    }
}

void scientistRepository::writeToRepository()
{
    ofstream fout;
    string output;
    int sizeOflist = list.size();

    fout.open("repository.txt", ios::out);

    if(fout.fail())
        cout << "ERROR: Cannot write to repository.\n";

    else if(fout.is_open())
    {
        for(int i = 0; i < sizeOflist; i++)
        {
            output = dataToString(list[i]);
            fout << output;
        }
    }
}

void scientistRepository::sortInput(string& entryInput, string& name, string& gender, int& birth, int& death)
{
    int inputLength = entryInput.length();
    string data;
    vector<string> temp;

    for(int i = 0; i < inputLength; i++)
    {
        if(entryInput[i] == '*')
        {
            i++;
            data = getData(i, entryInput);
            temp.push_back(data);
        }
    }

    name = temp[0];
    gender = temp[1];
    birth = stringToInt(temp[2]);
    death = stringToInt(temp[3]);
}

string scientistRepository::getData(int& index, const string& entryString)
{
    string temp = "";

    while(entryString[index] != '*')
    {
        temp += entryString[index];
        index++;
    }

    return temp;
}

int stringToInt(const string& str)
{
    int returnVal = 0;
    int multiplier = 1;
    vector<int> digits;
    int digit;
    int strLength = str.length();
    int howManyDigits;
    bool isNeg = false;

    if(str[0] == '-')
        isNeg = true;

    for(int i = 0 + isNeg; i < strLength; i++)
    {
        digit = str[i] - '0';
        digits.push_back(digit);
    }

    howManyDigits = digits.size();

    for(int i = howManyDigits - 1; i >= 0; i--)
    {
        returnVal += digits[i] * multiplier;
        multiplier *= 10;
    }

    if(isNeg)
        return(-returnVal);
    else
        return returnVal;
}

string scientistRepository::dataToString(computerScientist scientist)
{
    string temp;
    temp = '*' + scientist.getName() + '*' + " ";
    temp += '*' + scientist.getGender() + '*' + " ";
    temp += '*' + intToString(scientist.getBirthYear()) + '*' + " ";
    temp += '*' + intToString(scientist.getDeathYear()) + '*' + '\n';

    return temp;
}

string intToString(const int& integer)
{
    int tempInt = integer;
    string temp = "";
    char digit;

    bool isNeg = false;

    if(tempInt < 0)
    {
        isNeg = true;
        tempInt = -tempInt;
    }

    if(tempInt == 0)
        return "0";

    while(tempInt > 0)
    {
        digit = (tempInt % 10) + '0';
        temp += digit;
        tempInt /= 10;
    }

    temp = reverseString(temp);

    if(isNeg)
        return('-' + temp);
    else
        return temp;
}

string reverseString(const string& str)
{
    string temp = "";
    int sizeOfString = str.length();

    for(int i = sizeOfString -1; i >= 0; i--)
    {
        temp += str[i];
    }

    return temp;
}

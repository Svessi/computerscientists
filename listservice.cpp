#include "listservice.h"
#include <algorithm>

const int ERROR = -1;

listService::listService()
{
}

computerScientist listService::getScientist(const int& listIndex)
{
    return repository.getScientist(listIndex);
}

void listService::save()
{
    repository.writeToRepository();
}

bool listService::add(computerScientist& entry)
{
    string entryName = entry.getName();
    int index = getScientistIndex(entryName);

    if(index != ERROR)
        return false;

    repository.push(entry);
    return true;
}

void listService::remove(const int& index)
{
    vector<computerScientist> newList;
    int oldListSize = repository.size();

    for(int i = 0; i < oldListSize; i++)
    {
        if(i != index)
            newList.push_back(repository.getScientist(i));
    }

    repository.overWriteList(newList);
}

int listService::getScientistIndex(const string& nameOfEntry)
{
    int repositorySize = repository.size();
    computerScientist temp;

    for(int i = 0; i < repositorySize; i++)
    {
        temp = repository.getScientist(i);

        if(temp.getName() == nameOfEntry)
        {
            return i;
        }
    }

    return ERROR;
}

vector<computerScientist> listService::searchResults(const string& input)
{
    vector<computerScientist> list = repository.getList();
    vector<computerScientist> returnList;
    computerScientist object;
    vector<string> searchInput = splitString(input);

    int listSize = list.size();
    int numberOfInput = searchInput.size();

    for(int i = 0; i < listSize; i++)
    {
        object = list[i];

        for(int j = 0; j < numberOfInput; j++)
        {
            if(isSubString(object.getName(), searchInput[j]))
            {
                returnList.push_back(object);
                break;
            }
            else if(object.getGender() == searchInput[j])
            {
                returnList.push_back(object);
                break;
            }
            else if(object.getBirthYear() == stringToInt(searchInput[j]))
            {
                returnList.push_back(object);
                break;
            }
            else if(object.getDeathYear() == stringToInt(searchInput[j]))
            {
                returnList.push_back(object);
                break;
            }
        }
    }

    return returnList;
}

vector<computerScientist> listService::getSortedList(const int& display_case)
{
    vector<computerScientist> temp = repository.getList();

    switch(display_case)
    {
    case 1:
        sort(temp.begin(), temp.end(), nameCompare);
        break;
    case 2:
        sort(temp.begin(), temp.end(), genderCompare);
        break;
    case 3:
        sort(temp.begin(), temp.end(), birthCompare);
        break;
    case 4:
        sort(temp.begin(), temp.end(), deathCompare);
        break;
    default:
        break;
    }

    return temp;
}

bool nameCompare(computerScientist s1, computerScientist s2)
{
    return (s1.getName() < s2.getName());
}

bool genderCompare(computerScientist s1, computerScientist s2)
{
    return (s1.getGender() < s2.getGender());
}

bool birthCompare(computerScientist s1, computerScientist s2)
{
    return (s1.getBirthYear() < s2.getBirthYear());
}

bool deathCompare(computerScientist s1, computerScientist s2)
{
    return (s1.getDeathYear() < s2.getDeathYear());
}

vector<string> splitString(const string& str)
{
    vector<string> temp;
    string vectorInput = "";
    int stringLength = str.length();
    bool wasLastSpace = true;

    for(int i = 0; i < stringLength; i++)
    {
        if(isspace(str[i]))
        {
            if(wasLastSpace == false)
            {
                temp.push_back(vectorInput);
                vectorInput = "";
            }
            wasLastSpace = true;
            continue;
        }

        wasLastSpace = false;
        vectorInput += str[i];
    }

    if(vectorInput.length() != 0)
        temp.push_back(vectorInput);

    return temp;
}

bool isSubString(const string& str, const string& subStr)
{
    vector<string> stringSplit = splitString(str);
    int size = stringSplit.size();

    for(int i = 0; i < size; i++)
    {
        if(subStr == stringSplit[i])
            return true;
    }

    return false;
}

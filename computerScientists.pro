#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T21:28:34
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = computerScientists
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    computerscientist.cpp \
    consoleui.cpp \
    listservice.cpp \
    scientistrepository.cpp

HEADERS += \
    computerscientist.h \
    consoleui.h \
    listservice.h \
    scientistrepository.h

OTHER_FILES += \
    repository.txt \
    commands.txt

#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "computerscientist.h"
#include "listservice.h"

using namespace std;

class consoleUI
{
public:
    consoleUI();
    void start();
private:
    listService serviceLayer;
    vector<string> commandList;
    void initCommandList();
    void printCommandDescpriptions();
    void processCommand(const string &command);
    void processAdd();
    void processRemove();
    void processSearch();
    void processDisplay();
    void processSave();
    void processEdit();
};

void makeEntry(computerScientist &entry);
bool makeYear(int& year);
bool makeGender(string& gender);
bool makeName(string& name);
string capitalizeInitials(const string& str);
bool printListOfScientist(vector<computerScientist> &list);
bool isNameValid(const string& name);
bool isYearValid(const string& year);
void edit(string& name, string& gender, int& birthYear, int& deathYear);
string askForName();
string askForGender();
int askForBirthYear();
int askForDeathYear(const int& birthYear);
ostream& operator << (ostream& out, computerScientist a);

#endif // CONSOLEUI_H

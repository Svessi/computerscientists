#include <iostream>
#include "consoleui.h"

using namespace std;

int main()
{
    consoleUI interface;
    interface.start();

    return 0;
}

#ifndef SCIENTISTREPOSITORY_H
#define SCIENTISTREPOSITORY_H

#include <iostream>
#include <fstream>
#include <vector>
#include "computerscientist.h"

using namespace std;

class scientistRepository
{
public:
    scientistRepository();
    ~scientistRepository();
    computerScientist getScientist(const int &listIndex);
    vector<computerScientist> getList();
    void writeToRepository();
    void push(const computerScientist &scientist);
    int size();
    void overWriteList(vector<computerScientist> newList);
private:
    vector<computerScientist> list;
    void readRepository();
    string dataToString(computerScientist scientist);
    string getData(int &index, const string &entryString);
    void sortInput(string &entryInput, string &name, string &gender, int &birth, int &death);
};

int stringToInt(const string& str);
string intToString(const int& integer);
string reverseString(const string& str);

#endif // SCIENTISTREPOSITORY_H

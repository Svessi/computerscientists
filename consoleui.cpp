#include "consoleui.h"
#include <string>

const int ERROR = -1;

consoleUI::consoleUI()
{
    initCommandList();
}

void consoleUI::start()
{
    string command;

    cout << "Welcome to the incomprehensive list of famous computer scientists.\n"
         << "To continue, write a command.\nTo view all the commands available to you write: listCommands\n\n";

    do
    {
        command = "";

        cout << "command: ";
        getline(cin,command);

        processCommand(command);

    }while(command != "quit");
}

void consoleUI::initCommandList()
{
    ifstream fin;
    string command;
    char character;

    fin.clear();

    fin.open("commands.txt", ios::in);

    if(fin.fail())
        exit(1);

    if(fin.is_open())
    {

       while(!fin.eof())
       {
           command = "";

           fin.get(character);

           if(character == '*')
           {
               fin.get(character);

               while(character != '*' && !fin.eof())
               {
                   command += character;
                   fin.get(character);
               }

               commandList.push_back(command);
           }
       }
    }
    fin.close();
}

void consoleUI::printCommandDescpriptions()
{
    ifstream fin;
    string description = "";
    char character;

    fin.open("commands.txt", ios::in);

    if(fin.fail())
        exit(1);

    else if(fin.is_open())
    {
        while(!fin.eof())
        {
            fin.get(character);

            if(character != '*')
                description += character;
        }
    }

    fin.close();

    cout << endl << description << endl;
}

void consoleUI::processCommand(const string& command)
{
    if(command == commandList[0])   // listCommands
        printCommandDescpriptions();

    else if(command == commandList[1]) //add
        processAdd();

    else if(command == commandList[2]) //remove
        processRemove();

    else if(command == commandList[3]) //edit
        processEdit();

    else if(command == commandList[4]) //search
        processSearch();

    else if(command == commandList[5]) //display
        processDisplay();

    else if(command == commandList[6]) //Save
        processSave();

    else if(command == commandList[7]) //clear
        system("cls");

    else if(command != "" && command != commandList[8])
        cout << "\nThe program did not recognize the command: " << command << endl << endl;
}

void consoleUI::processAdd()
{
    string input;
    computerScientist entry;
    bool success;

    makeEntry(entry);

    cout << "\nYou have created the following entry:\n\n";
    cout << entry << endl;
    cout << "Do you want to add it to the list (Y/N)?: ";
    getline(cin,input);

    if(input == "y" || input == "Y")
    {
        success = serviceLayer.add(entry);

        if(success)
            cout << "\nThe scientist has been successfully added to the list.\n\n";
        else
            cout << "\nThis scientist is already in the list.\n\nAdding operation aborted.\n\n";
    }
    else
        cout << "\nEntry discarded.\n";

}

void consoleUI::processRemove()
{
    string input;
    int index;

    cout << "\nPlease enter the name of the scientist you want to remove from the list:\n\n";
    cout << "Name: ";
    getline(cin, input);

    input = capitalizeInitials(input);

    index = serviceLayer.getScientistIndex(input);

    if(index != ERROR)
    {
        cout << "\nThe following entry is the scientist you requested to remove:\n\n";
        cout << serviceLayer.getScientist(index) << endl;
        cout << "Are you sure you want to remove it (Y/N)?: ";
        getline(cin, input);

        if(input == "y" || input == "Y")
        {
            serviceLayer.remove(index);
            cout << "\nThe scientist has been successfully removed from the list.\n\n";
        }
        else
            cout << "\nRemove operation aborted.\n\n";
    }
    else
        cout << "\nThe entry you requested to remove does not exist.\n\n";

}

void consoleUI::processEdit()
{
    computerScientist temp;
    string name, gender, input;
    int birthYear, deathYear, index;
    bool isAgain;

    cout << "\nPlease enter the name of the scientist you want to edit:\n\n";
    cout << "Name: ";
    getline(cin, input);

    input = capitalizeInitials(input);

    index = serviceLayer.getScientistIndex(input);

    if(index != ERROR)
    {
        temp = serviceLayer.getScientist(index);

        name = temp.getName();
        gender = temp.getGender();
        birthYear = temp.getBirthYear();
        deathYear = temp.getDeathYear();

        cout << "\nThe following entry is the scientist you requested to edit:\n\n";
        cout << temp << endl;

        do
        {
            edit(name, gender, birthYear, deathYear);

            cout << "\nDo you want to continue editing this scientist(Y/N)?:";
            getline(cin,input);

            if(input == "y" || input == "Y")
                isAgain = true;
            else
                isAgain = false;

        }while(isAgain);

        cout << "\nEditing done.\n\n";
    }
    else
        cout << "\nThe entry you requested to edit does not exist.\n\n";

    temp.edit(name, gender, birthYear, deathYear);
    serviceLayer.remove(index);
    serviceLayer.add(temp);
}

void consoleUI::processSearch()
{
    string input;
    string copyInput;
    vector<computerScientist> list;
    int listSize;
    bool didPrint;

    cout << "\nWhat do you want to search for?\n\n";
    cout << "Search: ";
    getline(cin, input);

    copyInput = input;
    input = capitalizeInitials(input);

    list = serviceLayer.searchResults(input);
    listSize = list.size();

    if(listSize > 0)
        cout << "\nYour search result:\n";

    didPrint = printListOfScientist(list);

    if(!didPrint)
        cout << "Your search -" << copyInput << "- did not match any scientist.\n\n";

}

void consoleUI::processDisplay()
{
    string displayType = "";
    int displayCase = 0;
    bool didPrint;
    vector<computerScientist> temp;

    cout << "\nHow do you want to display the list?:\n\n";
    cout << "\t1:alphabetical\n\t2:gender\n\t3:birth\n\t4:death\n\n";
    cout << "display type: ";

    getline(cin, displayType);


    if (displayType == "alphabetical" || displayType == "Alphabetical" || displayType == "1"){
        displayCase = 1;
    }

    else if (displayType == "gender" || displayType == "Gender" || displayType == "2"){
        displayCase = 2;
    }

    else if (displayType == "birth" || displayType == "Birth" || displayType == "3"){
        displayCase = 3;
    }

    else if (displayType == "death" || displayType == "Death" || displayType == "4"){
        displayCase = 4;
    }

    else{
        cout << "\nError in input" << endl;
    }

    temp = serviceLayer.getSortedList(displayCase);

    didPrint = printListOfScientist(temp);

    if(!didPrint)
        cout << "The list is empty. Please add computer scientists to it!\n\n";
}

void consoleUI::processSave()
{
    serviceLayer.save();
    cout << "\nChanges to the list have been saved.\n\n";
}

void makeEntry(computerScientist& entry)
{
    string name, gender;
    int birthYear, deathYear;

    cout << "\nPlease insert the following information about the computer scientist:\n\n";
    cout << "\t(*):required information\n";
    cout << "\tYou can skip entering unneccesery information\n\tby hitting the Enter button.\n\n";

    name = askForName();
    name = capitalizeInitials(name);
    gender = askForGender();
    birthYear = askForBirthYear();
    deathYear = askForDeathYear(deathYear);

    entry.edit(name,gender,birthYear,deathYear);
}

string capitalizeInitials(const string& str)
{
    int stringLength = str.length();
    bool lastCharSpace = true;

    string newString = "";

    for(int i = 0; i < stringLength; i++)
    {
        if(isspace(str[i]))
        {
            if(lastCharSpace)
                continue;
            else
            {
                lastCharSpace = true;
                newString += str[i];
            }
        }
        else if(isalpha(str[i]) || isdigit(str[i]))
        {
            if(lastCharSpace)
            {
                newString += toupper(str[i]);
                lastCharSpace = false;
            }
            else
                newString += tolower(str[i]);
        }
    }

    return newString;
}

bool isNameValid(const string& name)
{
    int nameLength = name.length();
    int alphaCount = 0;

    for(int i = 0; i < nameLength; i++)
    {
        if(isdigit(name[i]))
        {
            cout << "\nNames do not include digits\n";
            return false;
        }
        else if(isalpha(name[i]))
            alphaCount++;
    }

    if(alphaCount > 0)
        return true;
    else
        return false;
}

bool isYearValid(const string& year)
{
    int stringLength = year.length();

    for(int i = 0; i < stringLength; i++)
    {
        if(!isdigit(year[i]))
        {
            cout << "\nYears must be represented only as a positive integer.\n";
            return false;
        }
    }

    return true;
}

bool printListOfScientist(vector<computerScientist>& list)
{
    int listSize = list.size();
    string line = "-----------------------------\n";

    cout << endl;

    if(listSize < 1)
        return false;

    cout << line;

    for(int i = 0; i < listSize; i++)
    {
        cout << list[i];
        cout << line;
    }

    cout << endl;

    return true;
}

void edit(string& name, string& gender, int& birthYear, int& deathYear)
{
    string input;

    cout << "What do you want to edit?\n\n\t1:Name\n\t2:Gender\n\t3:Birth year\n\t4:Death year\n\n";
    cout << "Edit: ";

    getline(cin,input);

    cout << endl;

    if (input == "name" || input == "Name" || input == "1"){
        name = askForName();
        name = capitalizeInitials(name);
    }

    else if (input == "gender" || input == "Gender" || input == "2"){
        gender = askForGender();
    }

    else if (input == "birth year" || input == "Birth year" || input == "3"){
        birthYear = askForBirthYear();
    }

    else if (input == "death year" || input == "Death year" || input == "4"){
        deathYear = askForDeathYear(birthYear);
    }

    else
        cout << "invalid input.\n\n";
}

string askForName()
{
    bool isValid;
    string name;

    do
    {
        cout << "*Name: ";
        getline(cin, name);

        isValid = isNameValid(name);

        if(!isValid)
            cout << "\nWrong Input, please enter again.\n\n";

    }while(!isValid);

    return name;
}

string askForGender()
{
    bool isValid;
    string gender;

    do
    {
        cout << "*Gender(Male/Female): ";
        getline(cin, gender);
        gender = capitalizeInitials(gender);

        if(gender != "Male" && gender != "Female")
        {
            isValid = false;
            cout << "\nWrong Input, please enter again.\n\n";
        }
        else
            isValid = true;

    }while(!isValid);

    return gender;
}

int askForBirthYear()
{
    bool isValid;
    int birthYear;

    do
    {
        cout << "Year of birth: ";
        isValid = makeYear(birthYear);

        if(!isValid)
            cout << "\nWrong Input, please enter again.\n\n";

    }while(!isValid);

    return birthYear;
}

int askForDeathYear(const int& birthYear)
{
    bool isValid;
    int deathYear;

    do
    {
        cout << "Year of death: ";
        isValid = makeYear(deathYear);

        if(deathYear < birthYear && deathYear != -1)
        {
            isValid = false;
            cout << "\nA person cannot die before she is born.\n";
            cout << "\nWrong Input, please enter again.\n\n";
        }

    }while(!isValid);

    return deathYear;
}

bool makeYear(int& year)
{
    string tempYear;
    bool isValid;

    getline(cin, tempYear);

    isValid = isYearValid(tempYear);

    if(tempYear == "")
        tempYear = "-1";

    year = stringToInt(tempYear);

    return isValid;
}

ostream& operator << (ostream& out, computerScientist a)
{
    out << "Name: " << "\t\t" << a.getName() << endl;
    out << "Gender: "<< "\t" << a.getGender() << endl;

    if(a.getBirthYear() != ERROR)
        out << "Birth Year: " << "\t" << a.getBirthYear() << endl;
    else
        out << "Birth Year: " << "\t" << "N/A\n";

    if(a.getDeathYear() != ERROR)
        out << "Death year: " << "\t" << a.getDeathYear() << endl;
    else
        out << "Death year: " << "\t" << "N/A\n";

    return out;
}

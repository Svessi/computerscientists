#ifndef LISTSERVICE_H
#define LISTSERVICE_H

#include <iostream>
#include "scientistrepository.h"

using namespace std;

class listService
{
public:
    listService();
    bool add(computerScientist& entry);
    void remove(const int &index);
    int getScientistIndex(const string& nameOfEntry);
    computerScientist getScientist(const int &listIndex);
    void save();
    vector<computerScientist> getSortedList(const int &display_case);
    vector<computerScientist> searchResults(const string& input);
private:
    scientistRepository repository;
};

vector<string> splitString(const string& str);
bool isSubString(const string& str, const string& subStr);
bool genderCompare(computerScientist s1, computerScientist s2);
bool nameCompare(computerScientist s1, computerScientist s2);
bool birthCompare(computerScientist s1, computerScientist s2);
bool deathCompare(computerScientist s1, computerScientist s2);

#endif // LISTSERVICE_H

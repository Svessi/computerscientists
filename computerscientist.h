#ifndef COMPUTERSCIENTIST_H
#define COMPUTERSCIENTIST_H

#include <iostream>
#include <string>

using namespace std;

class computerScientist
{
public:
    computerScientist();
    void edit(string sName, string sGender, int sBirthYear, int sDeathYear);
    string getName();
    string getGender();
    int getBirthYear();
    int getDeathYear();
private:
    string name;
    string gender;
    int birthYear;
    int deathYear;
};

#endif // COMPUTERSCIENTIST_H
